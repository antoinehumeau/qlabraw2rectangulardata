#' Plot radial displacement depending on time
#'
#' @param new_raw_data  a data.frame or matrix containing new raw data. Use for the plot
#' @param prediction a data.table of prediction, from qlab_predict.
#' @param choice "raw" (default) to plot radial displacement in cm and time in second or "relative" to plot radial displacement and time in %.
#' @param lwd thikness of lines
#' @param add.title If TRUE (default), add the id as title of the figure
#' @param add.prediction If TRUE(default), add the prediction result
#' @param prediction.cex character expansion factor for the prediction result (cex in mtext)
#' @details the id column should be names "id"
#'
#' @export
#'
qlab_plot <- function(new_raw_data, prediction, choice = "raw", lwd = 1, add.title = TRUE, add.prediction = TRUE,  prediction.cex = 1){
  if (!choice %in% c("raw", "relative")) stop("choice should be raw or relative")
  if (nrow(prediction) != 1) stop("predictions should be a data.table with one row exactely")
  vars <- c("MAS","MIS", "MA", "MI", "MAL", "MIL")

  #col2plot <- c(MAS = "#ffff33ff", MIS = "#e41a1cff", MA = "#58FAF4ff",  MI = "#0000FFff", MAL =  "#4daf4aff", MIL = "#984ea3ff")
  #col2plot <- c(MAS = "#8c510aff", MIS = "#d8b365ff", MA = "#f6e8c3ff",  MI = "#c7eae5ff", MAL =  "#5ab4acff", MIL = "#01665eff")
  col2plot <- c(MIS =  "#d9f0d3ff", MI = "#7fbf7bff",  MIL = "#1b7837ff", MAL = "#762a83ff"  , MA = "#af8dc3ff" , MAS = "#e7d4e8ff")

  if (choice == "raw") {
    var2plot <- paste0("endo_", stringr::str_to_lower(vars), "_radial_displ")
    ymax <- max(new_raw_data[,.SD, .SDcols = var2plot], na.rm = T)
    thex <- new_raw_data$time
    xmin <- min(new_raw_data$time, na.rm = T)
    xmax <- max(new_raw_data$time, na.rm = T)
    thexlab <- "Time (s)"
    theylab <- "Radial displacement (cm)"
    pred_at <- (xmin + xmax) / 2
    result_at <- pred_at + 0.01 * (xmax - xmin)
  } else {
    var2plot <- paste0("endo_", stringr::str_to_lower(vars), "_radial_displ_rel")
    ymax <- 100
    thex <- new_raw_data$time_rel
    xmin <- 0
    xmax <- 100
    thexlab <- "Relative time (%)"
    theylab <- "Radial displacemnt (%)"
    pred_at <- 50
    result_at <- 51

  }
  names(var2plot) <- vars

  ymin <- min(new_raw_data[,.SD, .SDcols = var2plot], na.rm = T)

  plot(1,1, type = "n", xlim = c(xmin, xmax), ylim = c(min(ymin,0), ymax), bty = "l", xlab = thexlab, ylab = theylab, font.lab = 2)

  cache <- lapply(1:length(var2plot), function(k) {
    they <- new_raw_data[, get(var2plot[k])]
    graphics::lines(x = thex[!is.na(they)], y = they[!is.na(they)], col = col2plot[k], lwd = lwd)

  })

  if (ymin < 0) graphics::abline(h = 0, lty = 2, col = "grey")

  graphics::legend(x = "topright", names(var2plot), text.col = col2plot, bty = "n", adj = 1)

  if (add.title == TRUE) {
    graphics::title(main = paste("ID :", prediction$id), line = 3)
  }

  if (add.prediction == TRUE) {
    graphics::mtext(text = "Prediction          Class\nof ACP        Probability", line = 0, adj = 1, at = pred_at , font = 2, cex = prediction.cex)

    graphics::mtext(text = paste0("= ", prediction$ACP_class, "\n= ", round(prediction$ACP_probability, 3)), line = 0, adj = 0, at = result_at, font = 2, cex = prediction.cex)
  }
}
