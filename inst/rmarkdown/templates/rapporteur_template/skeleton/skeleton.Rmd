---
title: "Cleaning qlab raw data files"
author: '`r author`'
date: '`r Sys.time()`'
output:
  html_document:
    code_folding: hide
    fig_caption: yes
    highlight: tango
    number_section: yes
    theme: cosmo
    toc: yes
    toc_float: yes
    keep_md: no
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
load_packs <- c("DT")
sapply(load_packs, library, character.only = T)

wrap_dt <- function(dt, ...){
   para <- as.list(formals(DT::datatable))

   para$class <- "display compact"
   para$rownames = FALSE
   para$filter = "bottom"
   para$autoHideNavigation = FALSE
   para$escape = TRUE
   para$options <- list(scrollX = TRUE, scrollY = FALSE, autowidth = TRUE)
   input_list <- list(...)

   for (i in setdiff(names(input_list), "options")) {
      para[i] <- input_list[[i]]
   }

   for (i in names(input_list$options)) {
       para$options[i] <- input_list$options[[i]]
   }

   para$data <- dt
   do.call(what = datatable, para)
}
```

# Summary

```{r summary-info}
n_input <- length(input_path)
n_input_custom <- ifelse(n_input == 1, "file", "files")
```

You transformed `r n_input` `r n_input_custom` from the qlab software into rectangular data files.

# Metadata

The metadata included `r ncol(all_metadata)` different columns : `r knitr::combine_words(colnames(all_metadata))`.

```{r metadata-missing, results = "asis"}
missing_meta_id <- which(is.na(all_metadata$id))
if (length(missing_meta_id) > 0) {
  cat("<span style='color:red'> Some metadata have missing ids. </span>  \n")
}

missing_meta_output <- which(is.na(all_metadata$output_name))
if (length(missing_meta_output) > 0) {
  cat("<span style='color:red'> Some metadata have missing output name. </span>  \n")
}
```

```{r metadata-table, results  ="asis"}
summary_metadata <- all_metadata[,.N, c("output_name","unit")]
wrap_dt(summary_metadata, caption = "Summary of metadata.")
metadata_bad_N <- which(summary_metadata$N != n_input)
if (length(metadata_bad_N) > 0 ) {
   cat(paste0("<span style='color:red'> The following metadata are not found exactly one by id: ", knitr::combine_words(summary_metadata[metadata_bad_N, output_name]), ".</span>  \n"))
}
wrap_dt(all_metadata, caption = "The metadata kept in the output")
```

# Data

The data included `r ncol(all_data)` different columns : `r knitr::combine_words(colnames(all_data))`.

```{r ids-mtedata-data, results = "asis"}
thedata_ids <- unique(all_data$id)
orphan_data <- setdiff(thedata_ids, all_metadata$id)

if (length(orphan_data) > 0 ) {
   cat(paste0("<span style='color:red'> The following ids are in the data but not in the metadata: ", knitr::combine_words(orphan_data), ".</span>  \n"))
}

orphan_metadata <- setdiff(all_metadata$id, thedata_ids)
if (length(orphan_metadata) > 0 ) {
   cat(paste0("<span style='color:red'> The following ids are in the metadata but not in the data: ", knitr::combine_words(orphan_metadata), ".</span>  \n"))
}

if (length(orphan_data) == 0 & length(orphan_metadata) == 0) {
   cat("All ids have both their data and metadata.  \n")
}
```

```{r nbydataT}
fin_time_var <- c("time", "duree")[which(c("time", "duree") %in% colnames(all_data))]

nbyid <- all_data[,.(N = .N, 
                     min = min(get(fin_time_var), na.rm = T),
                     max = max(get(fin_time_var), na.rm = T)
                     ), id]
nbyid[, total := max - min]
nbyid[, mean_lag := signif(total / N, 3)]
nbyid[, sd_lag := signif(total / N, 3)]
wrap_dt(nbyid, caption = "Number of data (N) by id and information about times (s). min and max are the minimal and maximal times for each id. total is max - min and mean_lag is total / N, the mean times between two data.")
```

```{r data-missing, results = "asis"}
na_by_col <- lapply(colnames(all_data), function(j) which(is.na(all_data[, get(j)])))
names(na_by_col) <- colnames(all_data)
cols_with_na <- names(na_by_col)[sapply(na_by_col, length) > 0]

if (length(cols_with_na) > 0) {
  ids_with_na <- all_data[unique(unlist(na_by_col)), sort(unique(id))]  
  times_with_na <- all_data[unique(unlist(na_by_col)), sort(unique(time))]
  cat("<span style='color:red'> The followings", length(cols_with_na), "variables contains missing data: ", paste0(knitr::combine_words(cols_with_na) , "."),  "</span>  \n")
  cat("<span style='color:red'> The followings", length(ids_with_na), "ids contains missing data: ", paste0(knitr::combine_words(ids_with_na) , "."),  "</span>  \n")
  cat("<span style='color:red'> The followings", length(times_with_na), "times contains missing data: ", paste0(knitr::combine_words(times_with_na) , "."),  "</span>  \n")
}

Ncol_plot_miss <- length(setdiff(colnames(all_data), c("id", "time")))
fig_asp <- 0.025 * Ncol_plot_miss
fig_cap <- NA
```

```{r plot-missing-time-function, eval = length(cols_with_na) > 0, include = length(cols_with_na) > 0}
fig_cap <- paste0("**", ids_with_na, "**: Missing data (black) by variable (y) and observation (x)")

plot_miss <- function(ids = ids_with_na, dt = all_data){
  cache = lapply(ids, function(theid){
    thedt <- dt[id == theid]
    Nt <- nrow(thedt)
    col_show <- setdiff(colnames(thedt), c("id"))
    
    rotate_p <- ifelse(nrow(thedt) > length(col_show), T, F)
    
    theplot <- visdat::vis_miss(thedt[, .SD, .SDcols = col_show], sort_miss = F) +
       ggplot2::ggtitle(theid) + 
      ggplot2::coord_flip()
    
    print(theplot)
  })
}
```

```{r plot-missing-time, eval = length(cols_with_na) > 0, fig.asp = fig_asp, fig.cap = fig_cap, include = length(cols_with_na) > 0}
plot_miss(ids = ids_with_na)
```

# Data engineering

The data engineering came from the data_engineering function and compute some variables for each profile. 

```{r check-engiy, results = "asis"}
n_sum <- nrow(all_summary)

eval_engi <- n_sum > 0
```

```{r noengi-text, results = "asis", eval = !eval_engi, include = !eval_engi }
cat("There is no summary in the data. If you need summary variables, you should engineering = TRUE in the main qlabtransform function.")
```

```{r engi-table, results  ="asis", eval = eval_engi, include = eval_engi}
orphan_data_sum <- setdiff(thedata_ids, all_summary$id)

if (length(orphan_data_sum) > 0 ) {
  cat(paste0("<span style='color:red'> The following ids are in the data but not in the summary: ", knitr::combine_words(orphan_data_sum), ".</span>  \n"))
}

orphan_summary <- setdiff(all_summary$id, thedata_ids)
if (length(orphan_summary) > 0 ) {
  cat(paste0("<span style='color:red'> The following ids are in the summary but not in the data: ", knitr::combine_words(orphan_summary), ".</span>  \n"))
}

if (length(orphan_data_sum) == 0 & length(orphan_summary) == 0) {
  cat("All ids have both their data and summary.  \n")
}

cat(paste0("The data engineering included ", ncol(all_summary), " different columns :", knitr::combine_words(colnames(all_summary)), "."))

all_summary2print <- copy(all_summary)
colnames2signif <- colnames(all_summary2print)[stringr::str_detect(colnames(all_summary2print), "(auc)|(time)|(area)")]
all_summary2print[, c(colnames2signif) := lapply(.SD, signif, 3), .SDcols = colnames2signif]

wrap_dt(all_summary2print, caption = "The data engineering output")
```

