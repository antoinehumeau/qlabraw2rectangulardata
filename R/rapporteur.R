#' Generate an html report from a list of files
#'
#' @param input_path vector of input names (same as in  \code{\link{raw2tidy}}).
#' @param list_tidy list of tidy data (output of  \code{\link{raw2tidy}}).
#' @param report_path path of the report. if null (default), the current path.
#' @param output_file Name of output file. Default is transformation_summary_ add with the today's date.
#' @param author Author name print on the report. Default is user name.
#' @seealso \code{\link{qlabtransform}}
#' @return a html document
#' @export
#'
rapporteur <- function(input_path, list_tidy, report_path = NULL, output_file = paste0("transformation_summary_", Sys.Date()), author = Sys.getenv("USERNAME")){
  all_data <- extract_data_list(list_tidy)
  all_metadata <- extract_metadata_list(list_tidy)
  all_summary <- extract_summary_list(list_tidy)

  template_path <- system.file("rmarkdown","templates","rapporteur_template","skeleton","skeleton.Rmd", package = "qlabRaw2RectangularData")

  if (is.null(report_path)) report_path <- getwd()

  rmarkdown::render(input = template_path, output_dir = report_path, output_file = output_file)
}
