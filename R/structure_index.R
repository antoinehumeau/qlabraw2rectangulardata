#' Identify the positions of different structural elements
#'
#' The structural element are header, core data and footer
#'
#' @param dt vector of characters
#' @param colname_pattern pattern used to find the elements in the vector correspond to the column names of the core data. use in \code{\link[stringr]{str_detect}}.
#' The default value of colname_pattern is chosen to protect from change in uppper lower case in the software and protect for encoding difficulties with accented letters in french for duree.
#'
#' @return list of 3 elements for positions of header, core_data and footer.
#' @details The function first search for non empty rows, i.e. rows with any digit or letter.
#' The header cut is the rows with maximum of --- .
#' Headers are the non empty rows before the header cut
#' The core data are non empty row the data between non consecutive  rows
#' The column names are find by detect dur.e or time at the begining of the text. The core data are data.
#' Footer are data after the last row of ---.

#' @export
#'
structure_index <- function(dt, colname_pattern = "^(([dD]ur.e)|([tT]ime))"){

  col_index <- col_rename <- NULL

  non_empty_rows <- unname(which(sapply(dt, stringr::str_detect, pattern = "[0-9a-zA-Z]")))
  empty_rows <- setdiff(1:length(dt), non_empty_rows)

  if (data.table::last(empty_rows) < length(dt)) empty_rows <- c(empty_rows, length(dt) + 1) # to capture the last row text if no empty

  rows_between_part <- which(stringr::str_count(dt, "[-]{3,}") > 0)

  header_cut <- min(rows_between_part)
  header_rows <- non_empty_rows[non_empty_rows < header_cut]

  which_colnames <- unname(which(sapply(dt, stringr::str_detect, pattern =  colname_pattern)))
  which_begin_empty <- which(diff(empty_rows) > 1)

  data_vars <- data.table::data.table(first = empty_rows[which_begin_empty] + 1,
                          last = empty_rows[which_begin_empty + 1] - 1)

  col_po <- apply(data_vars, 1, function(i) which_colnames[which_colnames > i["first"] & which_colnames <= i["last"]])
  col_po[sapply(col_po, length) == 0] <- NA

  data_vars[, col_index := unlist(col_po)]
  data_vars[, col_rename := col_index - 1]

  core_data <- data.table::rbindlist(lapply(which_colnames, function(k) {
    data.table::data.table(first = k + 1,
                           last = rows_between_part[rows_between_part > k][1] - 1,
                           col_index = k,
                           col_rename = last(non_empty_rows[non_empty_rows < k]))
  }), use.names = T, fill = T)
  footer_rows <- non_empty_rows[non_empty_rows > max(rows_between_part)]

  return(list(header = header_rows, core = core_data, footer = footer_rows  ))
}
