
# qlabRaw2RectangularData

The goal of qlabRaw2RectangularData is to transform raw data from the
Qlab13 software into rectangular data and metadata. It can save each
data and metadata and generate a report for the process.

The main function is qlabtransform that take input file paths, output
file paths and report path as main arguments. This function run in a row
then if save = T then if report = T.

## Installation

You can install from gitlab with:

``` r
remotes::install_gitlab("antoinehumeau/qlabraw2rectangulardata")
```

## Example

### Extract Data from qLab

The package was test with qLab version ?? for french and english
exports.

### Transform part

``` r
library(qlabRaw2RectangularData)

input_data_path <- file.path("A:", "20220808") 
name_input_files <- list.files(path = input_data_path, pattern = "([.]csv)$" ) 
path_input_files <- file.path(input_data_path, name_input_files)[1:5]

output_data_path <- file.path("..", "data", "data_valid") 
report_path <- file.path("..","reports") 

all_file_list <- qlabtransform(filepath = path_input_files, 
                               output_data_path = output_data_path, 
                               report_path = report_path )

all_data <- extract_data_list(all_file_list) # 1 data.table with all data
all_metadata <- extract_metadata_list(all_file_list) # 1 data.table with all metadata
```

### Analyse part

``` r
data_eng <- data_engineering(all_data, all_metadata)
```

# Shiny Application

The shiny application can be run with:

``` r
qlabRaw2RectangularData::qlab_shiny_app() 
```

# Notes

The imported data with with missing `r_avc` implies special exported
behaviour: The core data are exported but the metadata are not exported.

The used data from Qlab13 to create the package is in corrupted excel
format. They are opened via readLines. As it stands, an uncorrupted
Excel file is not managed.
