#' Compute and plot the break-down from Dalex
#'
#' @param new_summary a data.frame or matrix containing new data. It should be the summary output created from the data_engineering function
#' @param prediction_result data frame from qlab_predict to determine which explainer use
#' @param plot If True, plot the break-down results
#' @param max_features number of feature to plot
#' @param vcolors colors of positive and negative roles. If length is 2, final prediction color match with the class output and grey (#585858) is used to doubtful category
#'
#' @return the breakdown explainer from DALEX
#' @export
#' @import DALEXtra
#'
qlab_breakdownplot <- function(new_summary, prediction_result, plot = F, max_features = 5, vcolors = c("#91bfdb", "#fc8d59")){

  current_explainer_model <- switch(prediction_result$model,
                                    svm = svm_explainer,
                                    rf = rf_explainer,
                                    logreg = logreg_explainer,
                                    xgb = xgb_explainer)

  explain_result <- DALEX::predict_parts(explainer = current_explainer_model,
                                      new_observation = new_summary,
                                      type = "break_down")
  for (k in 1:length(translate_bdplot)) {
    explain_result$variable <- stringr::str_replace(explain_result$variable, names(translate_bdplot)[k], translate_bdplot[k])
  }

  if (plot == TRUE) {
    if (length(vcolors) == 2) {
      thecol <- switch(prediction_result$ACP_class,
                       "No ACP" = "#91bfdb",
                       "Doubtful" = "#585858",
                       "ACP" = "#fc8d59")
      vcolors <- c(vcolors, thecol)
    }
    bd_plot <- plot(explain_result, max_features = max_features, baseline = explain_result$contribution[1], min_max = c(0,1), vcolors = vcolors)
    print(bd_plot)
  }

  return(explain_result)
}
