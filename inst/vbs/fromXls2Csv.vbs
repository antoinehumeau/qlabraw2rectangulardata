'WScript.Echo "A"

Const TIMEOUT = 5
Set objShell = WScript.CreateObject("WScript.Shell")

'WScript.Echo "B"

if WScript.Arguments.Count < 2 Then
    WScript.Echo "Please specify the source and the destination files. Usage: ExcelToCsv <xls/xlsx source file> <csv destination file>"
    Wscript.Quit
End If

'WScript.Echo "C"
csv_format = 6

'WScript.Echo "D"
Set objFSO = CreateObject("Scripting.FileSystemObject")

'WScript.Echo "E"

src_file = objFSO.GetAbsolutePathName(Wscript.Arguments.Item(0))
dest_file = objFSO.GetAbsolutePathName(WScript.Arguments.Item(1))

'WScript.Echo "F"

Dim oExcel
Set oExcel = CreateObject("Excel.Application")

'WScript.Echo "G"
Dim oBook
Set oBook = oExcel.Workbooks.Open(src_file)

'WScript.Echo "H"
oExcel.DisplayAlerts = False
oBook.SaveAs dest_file, csv_format

'WScript.Echo "I"
oBook.Close False
oExcel.Quit

'WScript.Echo "J"
