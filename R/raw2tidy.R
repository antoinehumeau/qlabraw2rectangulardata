#' Transform raw csv data of qlab in rectangular data and metadata
#'
#' The raw csv data are transform into a rectangular data and metadata.
#'
#' @param filepath path of the data. can be a vector of file paths
#' @param id character to manage the id by \code{\link{find_id}}.
#' @param na.id character to manage the case of no id found. na.fail (default) return an error and na.pass return NA silently.
#' @param dec decimal separator. "auto" (default) searches for the separator in the file.
#' @param sep separator of columns. "auto" (default) searches for the separator in the file.
#' @param colname_pattern pattern used to find the elements in the vector correspond to the column names of the core data. use in \code{\link[stringr]{str_detect}}. #' The default value of colname_pattern is chosen to protect from change in uppper lower case in the software and protect for encoding difficulties with accented letters in french for duree.
#'
#' @param merge_metadata If TRUE merge the metadata to the main data. If a vector of characters, add the corresponding variables to the data.
#' @param col_header vector of characters: which input header saved in the metadata.
#' @details  Raw dara are read with readlines. The decimal then separator columns characters are first searched.
#' Then the structure of the document is find with structure_index function. The core data are transformed in tidy one. Header and footer are transformed in metadata.
#'
#' @return a list of size of the input filepath vector. Each element is a list of 2 data.table: data for the main database and metadata (1 row) for the main input metadata and extracted information.
#' @export
#' @seealso \code{\link{qlabtransform}}
#' @import data.table
#'
#' @examples
#' # Not run
#' # thefilepath <- file.path("..", "data", "namefile.csv")
#' # output <- raw2tidy(thefilepath)
raw2tidy <- function(filepath, id = "filename", na.id = "na.fail", dec = "auto", sep = "auto", colname_pattern = "^(([dD]ur.e)|([tT]ime))", merge_metadata = F, col_header = c("Date")){
  V1 <- V2 <- col_index <- col_rename <- complet_rename <- output <- input <- output_name <-  NULL

  if (length(filepath) > 1) {
    out_l <- lapply(filepath, function(k) {
      print(paste("Processing of ", k))
      raw2tidy(k, dec = dec, sep = sep, colname_pattern = colname_pattern, id = id, na.id = na.id, merge_metadata = merge_metadata, col_header = col_header)
      })
    return(unlist(out_l, recursive = F))
  } else {

    dt_raw <- load_raw_data(filepath)

    if (dec == "auto") dec <- search_dec(dt_raw)
    if (sep == "auto") sep <- search_sep(dt_raw)

    if (sep == dec) stop("dec(imal separator) and sep(arator of columns) cannot be the same")

    # find the structure of document
    dt_str <- structure_index(dt_raw, colname_pattern = colname_pattern)

    # extract the header, core and footer of the documents
    header_dt <- header_cleaning(dt_raw, dt_str$header, col_header = col_header, sep = sep)
    core_dt <- core_cleaning(dt_raw, dt_str$core, sep = sep)
    footer_dt <- footer_cleaning(dt_raw, dt_str$footer, sep = sep)
    theid <- find_id(char = id, path = filepath, header = header_dt, na.action = na.id)

    metadata_from_core_data <- core_dt$metadata

    metadata <- data.table::data.table(id = theid, data.table::rbindlist(list(metadata_from_core_data, header_dt,  footer_dt), use.names = T, fill = T))

    dt_tidy <- data.table::data.table(id = theid, core_dt$data)

    if (is.logical(merge_metadata)) {
      if (merge_metadata == T) {
        dt_tidy <- cbind(dt_tidy, metadata)
      }
    } else {
      bad_input <- setdiff(merge_metadata, colnames(metadata))
      if (length(bad_input) > 0 ) {
        stop(paste("The following variable are not metadata so cannot be merge:",
                   knitr::combine_words(bad_input), ". The available metadata are:",
                   knitr::combine_words(colnames(metadata))))
      }
      dt_tidy <- cbind(dt_tidy, metadata[, .SD, .SDcols = merge_metadata])
    }

    colnames(dt_tidy) <- qlab_translate(colnames(dt_tidy), lang_input = "auto", lang_output = "en")

    metadata[, output_name := qlab_translate(output_name, lang_input = "auto", lang_output = "en")]

    return(list(list(data = dt_tidy, metadata = metadata)))
  }
}
